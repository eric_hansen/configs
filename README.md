# Configuration Files

This repo keeps track of a bunch of configuration files that I use all
over the place.

```
configuration-files
├─ README.md
├─ ehansen
│  ├─ ai_microservice_configuration_files
│  │  ├─ bed-management-config-home.json
│  │  ├─ bed-management-config-root.json
│  │  └─ bed-management-config-whatever.json
│  ├─ logging-configs
│  │  ├─ logging-artisightmodeling.yaml
│  │  └─ logging-cv-modeling.yaml
│  ├─ manifests
│  │  └─ bed_management
│  │     ├─ bed-management-autosync-2.manifest
│  │     └─ bed-management-autosync.manifest
│  └─ vscode
│     └─ launch-cv-modeling.json
└─ mbarna
   ├─ hand-hygiene-ai-video-configjson
   └─ patient_ai
      ├─ fall-prevention-config.json
      ├─ hand-hygiene-ai-config.json
      ├─ hand-hygiene-capture-config.json
      ├─ hand-hygiene-capture-video-config.json
      ├─ or-service-config.json
      └─ pressure-ulcer-example.config.json

```